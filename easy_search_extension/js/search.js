document.addEventListener('DOMContentLoaded', function () {	
    getLatLong();
    backgroundImage();

	function convertImageToBase64(url, callback) {
	  var xhr = new XMLHttpRequest();
	  xhr.onload = function() {
	    var reader = new FileReader();
	    reader.onloadend = function() {
	      callback(reader.result);
	    }
	    reader.readAsDataURL(xhr.response);
	  };
	  xhr.open('GET', url);
	  xhr.responseType = 'blob';
	  xhr.send();
	}

    // Fetch background image
	 function backgroundImage(){
         fetch("https://source.unsplash.com/1600x900/?nature").then((response)=> {   
		     var status = response.status;
	         if(status=="200"){
	         	    console.log(response);
					convertImageToBase64(response.url, function(dataUrl) {
					  document.body.style.backgroundImage = "url('"+dataUrl+"')";
					})
	         }
		  })
	 }
     
    // Search result  
	var submitSearch = document.getElementById("search-form");
	submitSearch.addEventListener('submit',function (e){
		var searchBox = document.getElementById("search-bx");
		window.open("https://www.bing.com/search?q=" + searchBox.value);
	});


	function getLatLong(){          
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var obj = JSON.parse(this.responseText);
                var lat = obj.geoplugin_latitude;
                var long = obj.geoplugin_longitude;
                getWeather(lat,long);
            }
        };
        xhttp.open("GET", "http://www.geoplugin.net/json.gp", true);
        xhttp.send();
	}
    function getWeather(lat,long){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var obj = JSON.parse(this.responseText);
                console.log(obj);
                var desc = document.getElementById("weather-desc").innerHTML=obj.weather[0]["main"];
                var temp = document.getElementById("temprature").innerHTML=obj.main["temp"]+"c";
                var city = document.getElementById("city").innerHTML=obj.name;
                //var iconurl = "http://openweathermap.org/img/w/" + obj.weather[0]["icon"] + ".png";
                //var icon = "<img src='"+iconurl+"' class='weather-img'/>"
                var icon="<i class='owi owi-"+ obj.weather[0]["icon"]+"'></i>";
                document.getElementById("weather-bx-img").innerHTML=icon;
                
            }
        };
        xhttp.open("GET", "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+long+"&APPID=ee19e8936d962a9f8f6ac2e58903e8e6&units=metric", true);
        xhttp.send();
    }
}, false);