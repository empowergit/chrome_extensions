document.addEventListener('DOMContentLoaded', function () {
    var gmail_loader = document.getElementById("gmail_loader");
    /*var form2 = document.getElementById('search_web_txt_bx');
    form2.addEventListener('keypress', function (e) {
        if (form2.value != "") {
            if (e.keyCode === 13) {
                window.open("https://www.bing.com/search?q=" + form2.value, "_blank");
            }
        }
    });*/
    

    let form = document.getElementById('search_gmail_txt_bx');
    form.addEventListener('keypress', function (e) {
        if (form.value != "") {
            if (e.keyCode === 13) {
               gmail_loader.style.display = "block";  
               search_message(form.value);
            }
        }
    });
    
    var search_icon = document.getElementById('search_icon');
    search_icon.addEventListener('click', function (e) {
        if (form.value != "") {
               gmail_loader.style.display = "block";  
               search_message(form.value);
        }
    });	
    
    // var refresh = document.getElementById('refresh');
    // refresh.addEventListener('click', function (e) {
        // getToken();
    // });	
	
	getToken();

	function getToken(){
		chrome.identity.getAuthToken({ interactive: true }, function (token) {
			if (chrome.runtime.lastError) {
				console.log(chrome.runtime.lastError);
				return;
			}
			console.log(token);
			//localStorage.setItem("access_token", token);
			setUserInfo(token);
			setStorage("access_token", token);
		});
		chrome.identity.getProfileUserInfo(function(userInfo){
			console.log(userInfo);
		});
	}

    function search_message(subject) {
        var email_address = getStorage("email_address");
        var access_token = getStorage("access_token");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                message_detail(myArr, email_address, access_token);
            }
        };
        xhttp.open("GET", "https://www.googleapis.com/gmail/v1/users/" + email_address + "/messages?q=" + subject, true);
        xhttp.setRequestHeader("Authorization", "Bearer " + access_token);
        xhttp.send();
    }

    function setUserInfo(access_token) {
        /**Get G-mail Id**/
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                email_address = myArr.email;
                //localStorage.setItem("email_address", email_address);
                setStorage("email_address", email_address);
            }
        };
        xhttp.open("GET", "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + access_token, true);
        xhttp.send();
        /**Get G-mail Id**/
    }

    function setStorage(key, value) {
        console.log(chrome);
        localStorage.setItem(key, value);
    }
    function getStorage(key) {
        return localStorage.getItem(key);
    }

    function message_detail(json_res, email_address, access_token) {
        document.getElementById("gmail_msg_response").innerHTML = "";

        var result_size = json_res.resultSizeEstimate;
        if (result_size > 0) {
            var i, j, x = "";
            for (i = 0; i < json_res.messages.length; i++) {
                console.log(json_res.messages[i]["id"]);
                x = json_res.messages[i]["id"];
                console.log(x);
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        var myArr = JSON.parse(this.responseText);

                        for (j = 0; j < myArr.payload.headers.length; j++) {
                            var subkey = myArr.payload.headers[j].name;
                            if (subkey == "Subject") {
                                var subval = myArr.payload.headers[j].value;
                                if(subval != ""){
                                   var subval = myArr.payload.headers[j].value; 
                                }
							    else{
                                   var subval = "No Subject";
                                }
                            }
                        }

                        var html = "";
                        html += "<li>";
                        html += "<div class='message_description bg-mail'>";
                        html += "<a href='https://mail.google.com/mail/u/1/?pli=1#inbox/" + myArr.id + "' target='_blank'>" + subval + "</a>";
                        html += "</div>";
                        html += "</li>";
                        //document.getElementById("gmail_msg_response").innerHTML =html;
                        //$("#gmail_msg_response").append(html);
                        var emailList = document.getElementById("gmail_msg_response");
                        emailList.innerHTML += html;
                        gmail_loader.style.display = "none";
                    }
                };
                xhttp.open("GET", "https://www.googleapis.com/gmail/v1/users/" + email_address + "/messages/" + x, true);
                xhttp.setRequestHeader("Authorization", "Bearer " + access_token);
                xhttp.send();
            }
            if(json_res.messages.length > 10){
               var emailList = document.getElementById("gmail_msg_response");
               emailList.style.height = "402px";
               emailList.style.overflowY= "scroll";
            }else{
               var emailList = document.getElementById("gmail_msg_response");
               emailList.style.height = "";
               emailList.style.overflowY= "";  
            }
        } else {            
			var html = "";
			html += "<li>";
			html += "<div class='message_description bg-search'>";
			html += "No messages matched your search.";
			html += "</div>";
			html += "</li>";
			document.getElementById("gmail_msg_response").innerHTML = html;
            gmail_loader.style.display = "none";
        }
    }


}, false);




